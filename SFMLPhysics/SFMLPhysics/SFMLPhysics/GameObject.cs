﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using SFML.Graphics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics.Contacts;

namespace SFMLPhysics
{
    public class GameObject
    {
        private Body _body;
        private Fixture _fixture;
        private CircleShape _drawable;

        public GameObject(World world, int x, int y)
        {
            //FARSEER stuff
            //We create a body object and make it dynamic (movable)
            //_body = BodyFactory.CreateCircle(world, 10, 0.5f, new Vector2(x, y)); (this would attach a circle for you but then you don't have the fixture)
            _body = BodyFactory.CreateBody(world, new Vector2(x, y));
            //then we attach a circular shape to the body to give it a physical form
            _fixture = FixtureFactory.AttachCircle(10, 0.5f, _body);
            _fixture.OnCollision += OnCollision;
            
            //make the body dynamic so it can be affected by forces like gravity!
            _body.BodyType = BodyType.Dynamic;

            //SFML stuff
            //we create a drawable shape with the same radius as the physical shape that will be used to render the gameobject
            _drawable = new CircleShape(10);
            _drawable.Origin = new SFML.System.Vector2f(5, 5);
            _drawable.Position = new SFML.System.Vector2f(x, y);
        }

        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            _drawable.FillColor = Color.Red;
            return true;
        }

        internal void Draw(RenderWindow window)
        {
            //sync the drawable's position with the physical position
            _drawable.Position = new SFML.System.Vector2f(_body.Position.X, _body.Position.Y);
            //...and draw it!
            window.Draw(_drawable);
        }
    }
}
