﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFMLPhysics
{
    class Program
    {        
        static void Main(string[] args)
        {
            //SFML initialization
            bool running = true;
            //The context settings enable antialiasing which makes the circles much nicer
            RenderWindow window = new RenderWindow(new VideoMode(800, 600), "SFML + Farseer = <3", Styles.Default, new ContextSettings(24, 8, 8));
            //Vsync enabled
            window.SetVerticalSyncEnabled(true);
            //Thanks to this the window can be closed again ;)
            window.Closed += (a, b) => running = false;

            //Farseer initialization
            World world = new World(new Vector2(0, 9.82f)); //real world gravity constant
            //let's have a floor
            var floor = BodyFactory.CreateEdge(world, new Vector2(0, 300), new Vector2(800, 290));

            //GameObject initialization
            List<GameObject> objects = new List<GameObject>();
            //let's spawn a couple of gameobjects. they handle both their physical and visual representations!
            for (int x = 0; x < 800; x += 50)
                for(int y = 0; y < 200; y += 50)
                        objects.Add(new GameObject(world, x, y));

            //we'll use a stopwatch to measure time
            Stopwatch clock = new Stopwatch();
            clock.Start();

            //GAMELOOP
            while (running)
            {
                //how much time has passed?
                double dt = clock.ElapsedTicks / (double)Stopwatch.Frequency;
                clock.Restart();
                //Advance the physical simulation!
                //Physics are sensitive to too small or large timesteps but i wanted to keep this brief
                world.Step((float)dt);

                //update & draw gameobjects
                window.DispatchEvents();
                //clear the window
                window.Clear();
                //draw the gameobjects
                foreach (var obj in objects)
                    obj.Draw(window); //sync the drawable with the physical rep and then draw
                //display what has been drawn
                window.Display();
            }
        }
    }
}
